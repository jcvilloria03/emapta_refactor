<?php
function process($argv) {
    foreach (explode("\n", file_get_contents($argv[1])) as $row) {

        if (empty($row)) break;

        $p = explode(",",$row);
        for($x=0;$x<count($p);$x++){
            $p2 = explode(':', $p[$x]);
            $value[$x] = trim($p2[1], '"');
            if($x == 2){
                $value[$x] = substr($value[$x], 0, 3);
            }
        }
        $binResults = file_get_contents('https://lookup.binlist.net/' .$value[0]);
        
        if (!$binResults)
            die('error!');
        $r = json_decode($binResults);
        //var_dump($r->country->alpha2);die();
        $isEu = isEu($r->country->alpha2);
        $rate = @json_decode(file_get_contents('https://api.exchangeratesapi.io/latest'), true)['rates'][$value[2]];
        if ($value[2] == 'EUR' or $rate == 0) {
            $amntFixed = $value[1];
        }
        if ($value[2] != 'EUR' or $rate > 0) {
            $amntFixed = $value[1] / $rate;
        }
        $final_val = round_up($amntFixed * ($isEu == 'yes' ? 0.01 : 0.02),2);
        echo $final_val;
        print "\n";
    }
}

function isEu($c) {
    $result = false;
    switch($c) {
        case 'AT':
        case 'BE':
        case 'BG':
        case 'CY':
        case 'CZ':
        case 'DE':
        case 'DK':
        case 'EE':
        case 'ES':
        case 'FI':
        case 'FR':
        case 'GR':
        case 'HR':
        case 'HU':
        case 'IE':
        case 'IT':
        case 'LT':
        case 'LU':
        case 'LV':
        case 'MT':
        case 'NL':
        case 'PO':
        case 'PT':
        case 'RO':
        case 'SE':
        case 'SI':
        case 'SK':
            $result = 'yes';
            return $result;
        default:
            $result = 'no';
    }
    return $result;
}

function round_up ( $value, $precision ) { 
    $pow = pow ( 10, $precision ); 
    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
} 