<?php

use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    public function testisEu() {
        include_once 'src/app.php';
        $result = isEu('DK');

        $this->assertEquals('yes', $result);
    }
    
    public function testround_up() {
        include_once 'src/app.php';
        $result = round_up(0.46180844185832,2);
        //var_dump($result);die();
        $this->assertEquals('0.47', $result);
    }
    
}